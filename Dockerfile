FROM ubuntu:jammy
ENV OMP_NUM_THREADS=1 MAGICK_THREADS=1
RUN mkdir -p app/lib app/bin
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/Amsterdam"
RUN apt-get update && apt-get install -y liblog-log4perl-perl libmoo-perl libdbix-class-perl libjson-maybexs-perl libdatetime-perl libdatetime-format-iso8601-perl libdatetime-format-mysql-perl libtoml-perl libdbd-mysql-perl libstrictures-perl libfile-temp-perl libfile-which-perl libhtml-strip-perl libimage-magick-perl libdatetime-format-strptime-perl libmodule-install-perl libdata-printer-perl cpanminus make dumb-init git curl wget && apt-get clean
ADD ./config/docker_imagemagic_policy.xml /etc/ImageMagick-6/policy.xml
RUN cpanm -n Museum::Rijksmuseum::Object Mastodon::Client Museum::TePapa Museum::MetropolitanMuseumArt GraphQL::Client Museum::EICAS
RUN cd /tmp ; git clone https://github.com/eythian/perl-net-digitalnz.git && cd perl-net-digitalnz && perl Makefile.PL && make install && cd && rm -rf /tmp/perl-net-digitalnz
ADD bin app/bin
ADD lib app/lib
ADD t app/t
ENTRYPOINT ["dumb-init", "/app/bin/exhibitexplorer"]
