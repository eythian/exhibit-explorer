package App::ExhibitExplorer::SourceFactory;

use v5.34.0;
use strictures 2;

use Carp;
use Moo;

use App::ExhibitExplorer::Harvester::DigitalNZ;
use App::ExhibitExplorer::Harvester::Dummy;
use App::ExhibitExplorer::Harvester::EICAS;
use App::ExhibitExplorer::Harvester::MetropolitanMuseumArt;
use App::ExhibitExplorer::Harvester::Powerhouse;
use App::ExhibitExplorer::Harvester::Rijksmuseum;
use App::ExhibitExplorer::Harvester::TePapa;
use App::ExhibitExplorer::Publisher::Console;
use App::ExhibitExplorer::Publisher::Mastodon;

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::SourceFactory - Fetch the various harvester and publisher
information and modules based on names and IDs

=head1 VERSION

See L<App::ExhibitExplorer>

=cut

use App::ExhibitExplorer; our $VERSION = $App::ExhibitExplorer::VERSION;

=head1 SYNOPSIS

This makes it easy to map source IDs and names to the relevent modules, without
repeating logic all over the place. It also handles publishers, so really
should be called "SourceAndDestinationFactory".

    use App::ExhibitExplorer::SourceFactory;

    my $sf   = App::ExhibitExplorer::SourceFactory->new( schema => $schema );
    my $name = $sf->get_name_for_id(1);          # returns e.g. 'dummy' or 'rijksmuseum'
    my $id   = $sf->get_id_for_name('dummy');    # returns e.g. 1

    # These instantiate the relevant harvester for you, with the provided args.
    my $harvester = $sf->get_harvester_for_name( 'rijksmuseum', %args );
    my $harvester = $sf->get_harvester_for_id( 1, %args );

    # Same with publisher
    my $publisher = $sf->get_publisher_for_name( 'mastodon', %args );
    # (currently there's no need for publisher IDs)

=head1 SUBROUTINES/METHODS

=head2 new

    my $sf = App::ExhibitExplorer::SourceFactory->new( schema => $schema );
    
Instantiate the factory. C<schema> is an already-connected database schema that
it'll use to look up mappings.

=cut

sub BUILD {
    my ( $self, $args ) = @_;

    # We pre-cache the table because we don't expect the sources to change
    # under our feet.
    my @sources = $args->{schema}->resultset('Source')->search();
    my ( %id_to_name, %name_to_id );
    foreach my $s (@sources) {
        my $name = $s->name;
        my $id   = $s->id;
        $id_to_name{$id}   = $name;
        $name_to_id{$name} = $id;
    }
    $self->{_id_to_name} = \%id_to_name;
    $self->{_name_to_id} = \%name_to_id;
}

=head2 get_name_for_id
=head2 get_id_for_name

    my $name = $sf->get_name_for_id(1);          # returns e.g. 'dummy' or 'rijksmuseum'
    my $id   = $sf->get_id_for_name('dummy');    # returns e.g. 1

Given an ID (or a name) of a source, this returns the name (or ID), as defined
in the C<source> table of the database.

=cut

sub get_name_for_id {
    my ( $self, $id ) = @_;

    my $name = $self->{_id_to_name}{$id};
    return $name if $name;
    croak "Invalid source id '$id' specified";
}

sub get_id_for_name {
    my ( $self, $name ) = @_;

    confess "get_id_for_name: undefined name" unless $name;
    my $id = $self->{_name_to_id}{$name};
    return $id if defined $id;
    croak "Invalid source name '$name' specified";
}

=head2 get_harvester_for_name
=head2 get_harvester_for_id

    my $harvester = $sf->get_harvester_for_name( 'rijksmuseum', %args );
    my $harvester = $sf->get_harvester_for_id( 1, %args );

This returns an instantiated harvester object for the requested source. Any
extra values are passed directly to the constructor, though C<config> is
rewritten to just point to the segment for this module, and C<source_id> is
generated.

=cut

sub get_harvester_for_name {
    my ( $self, $name, %args ) = @_;

    $args{config}    = $args{config}{$name};
    $args{source_id} = $self->get_id_for_name($name);
    
    my $harv;
    if ( $name eq 'rijksmuseum' ) {
        $harv = App::ExhibitExplorer::Harvester::Rijksmuseum->new(%args);
    } elsif ( $name eq 'tepapa' ) {
        $harv = App::ExhibitExplorer::Harvester::TePapa->new(%args);
    } elsif ( $name eq 'metropolitan' ) {
        $harv = App::ExhibitExplorer::Harvester::MetropolitanMuseumArt->new(%args);
    } elsif ( $name eq 'powerhouse' ) {
        $harv = App::ExhibitExplorer::Harvester::Powerhouse->new(%args);
    } elsif ( $name eq 'eicas' ) {
        $harv = App::ExhibitExplorer::Harvester::EICAS->new(%args);
    } elsif ( $name eq 'digitalnz' ) {
        $harv = App::ExhibitExplorer::Harvester::DigitalNZ->new(%args);
    } elsif ( $name eq 'dummy' ) {
        $harv = App::ExhibitExplorer::Harvester::Dummy->new(%args);
    } else {
        croak "Invalid source name '$name' specified";
    }
    return $harv;
}

sub get_harvester_for_id {
    my ( $self, $id, %args ) = @_;

    my $name = $self->get_name_for_id($id);
    croak "Invalid source id '$id' specified" unless $name;

    $args{config}    = $args{config}{$name};
    $args{source_id} = $id;

    my $harv;
    if ( $name eq 'rijksmuseum' ) {
        $harv = App::ExhibitExplorer::Harvester::Rijksmuseum->new(%args);
    } elsif ( $name eq 'tepapa' ) {
        $harv = App::ExhibitExplorer::Harvester::TePapa->new(%args);
    } elsif ( $name eq 'metropolitan' ) {
        $harv = App::ExhibitExplorer::Harvester::MetropolitanMuseumArt->new(%args);
    } elsif ( $name eq 'powerhouse' ) {
        $harv = App::ExhibitExplorer::Harvester::Powerhouse->new(%args);
    } elsif ( $name eq 'eicas' ) {
        $harv = App::ExhibitExplorer::Harvester::EICAS->new(%args);
    } elsif ( $name eq 'digitalnz' ) {
        $harv = App::ExhibitExplorer::Harvester::DigitalNZ->new(%args);
    } elsif ( $name eq 'dummy' ) {
        $harv = App::ExhibitExplorer::Harvester::Dummy->new(%args);
    } else {

        # This probably means a database/code mismatch. Probably should
        # push this 'if' into the database some time, or have a more dynamic
        # loader.
        croak "Unable to find module for name '$name' (id: '$id')";
    }
    return $harv;
}

=head2 get_publisher_for_name

    my $publisher = $sf->get_publisher_for_name( 'mastodon', %args );

This returns an instantiated publisher object for the requested destination.
Any extra values are passed directly to the constructor, though C<config> is
rewritten to point to the publisher-specific entry.

=cut

sub get_publisher_for_name {
    my ( $self, $name, %args ) = @_;

    $args{config} = $args{config}{$name};
    my $pub;
    if ($name eq 'console') {
        $pub = App::ExhibitExplorer::Publisher::Console->new(%args);
    } elsif ($name eq 'mastodon') {
        $pub = App::ExhibitExplorer::Publisher::Mastodon->new(%args);
    } else {
        croak "Unable to find publisher for name '$name'";
    }
    return $pub;
}

has schema => (
    is       => 'ro',
    required => 1,
);

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::SourceFactory

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;    # End of App::ExhibitExplorer

