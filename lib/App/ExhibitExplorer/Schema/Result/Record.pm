use utf8;
package App::ExhibitExplorer::Schema::Result::Record;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

App::ExhibitExplorer::Schema::Result::Record

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<record>

=cut

__PACKAGE__->table("record");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 source_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

refers to source.id

=head2 subsource_id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 1

refers to subsource.id

=head2 record_id

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 record_timestamp

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 is_eligible

  data_type: 'tinyint'
  is_nullable: 0

=head2 ineligible_reason

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 is_used

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 used_timestamp

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 error

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 error_details

  data_type: 'text'
  is_nullable: 1

=head2 mysql_row_created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: 'current_timestamp()'
  is_nullable: 1

=head2 mysql_row_updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: 'current_timestamp()'
  is_nullable: 1

=head2 ordering

  data_type: 'double precision'
  default_value: 'rand()'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "source_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
  "subsource_id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 1,
  },
  "record_id",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "record_timestamp",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "description",
  { data_type => "text", is_nullable => 1 },
  "is_eligible",
  { data_type => "tinyint", is_nullable => 0 },
  "ineligible_reason",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "is_used",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "used_timestamp",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "error",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "error_details",
  { data_type => "text", is_nullable => 1 },
  "mysql_row_created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "current_timestamp()",
    is_nullable => 1,
  },
  "mysql_row_updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "current_timestamp()",
    is_nullable => 1,
  },
  "ordering",
  {
    data_type     => "double precision",
    default_value => "rand()",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<source_id>

=over 4

=item * L</source_id>

=item * L</record_id>

=back

=cut

__PACKAGE__->add_unique_constraint("source_id", ["source_id", "record_id"]);

=head1 RELATIONS

=head2 source

Type: belongs_to

Related object: L<App::ExhibitExplorer::Schema::Result::Source>

=cut

__PACKAGE__->belongs_to(
  "source",
  "App::ExhibitExplorer::Schema::Result::Source",
  { id => "source_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 subsource

Type: belongs_to

Related object: L<App::ExhibitExplorer::Schema::Result::Subsource>

=cut

__PACKAGE__->belongs_to(
  "subsource",
  "App::ExhibitExplorer::Schema::Result::Subsource",
  { id => "subsource_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2024-01-05 15:40:24
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:bZfOP+q72O/SsX+bH8361A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
