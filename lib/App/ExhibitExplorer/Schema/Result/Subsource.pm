use utf8;
package App::ExhibitExplorer::Schema::Result::Subsource;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

App::ExhibitExplorer::Schema::Result::Subsource

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<subsource>

=cut

__PACKAGE__->table("subsource");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 enabled

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 mysql_row_created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: 'current_timestamp()'
  is_nullable: 1

=head2 mysql_row_updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: 'current_timestamp()'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "enabled",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "mysql_row_created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "current_timestamp()",
    is_nullable => 1,
  },
  "mysql_row_updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "current_timestamp()",
    is_nullable => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 records

Type: has_many

Related object: L<App::ExhibitExplorer::Schema::Result::Record>

=cut

__PACKAGE__->has_many(
  "records",
  "App::ExhibitExplorer::Schema::Result::Record",
  { "foreign.subsource_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2024-01-05 15:40:24
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:4punbD/yoLR9f4KVMJPYKw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
