use utf8;
package App::ExhibitExplorer::Schema::Result::EventLog;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

App::ExhibitExplorer::Schema::Result::EventLog

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<event_log>

=cut

__PACKAGE__->table("event_log");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 source

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 level

  data_type: 'varchar'
  is_nullable: 0
  size: 10

=head2 event

  data_type: 'longtext'
  is_nullable: 0

=head2 mysql_row_created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: 'current_timestamp()'
  is_nullable: 1

=head2 mysql_row_updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: 'current_timestamp()'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "source",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "level",
  { data_type => "varchar", is_nullable => 0, size => 10 },
  "event",
  { data_type => "longtext", is_nullable => 0 },
  "mysql_row_created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "current_timestamp()",
    is_nullable => 1,
  },
  "mysql_row_updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "current_timestamp()",
    is_nullable => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2023-03-04 09:29:25
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:VjwaUdy2TyMkWpmbyFtmeQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
