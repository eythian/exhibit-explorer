package App::ExhibitExplorer::Utils::Network;

use v5.34.0;
use strictures 2;

use Exporter qw( import );
use LWP::Simple;
our @EXPORT_OK = qw( check_network );

=head1 NAME

App::ExhibitExplorer::Utils::Network - Utilities for network things

=head1 VERSION

See L<App::ExhibitExplorer>

=cut

use App::ExhibitExplorer; our $VERSION = $App::ExhibitExplorer::VERSION;

=head1 SYNOPSIS

    use App::ExhibitExplorer::Utils::Network qw( check_network )
    my $have_network = check_network;
    if ($have_network) {
        say "We seem to have internet";
    } else {
        say "We don't seem to have internet";
    }

=head1 SUBROUTINES

=head2 check_network

    my $have_network = check_network;

Does a simple check to see if we have network connectivity.

=cut

sub check_network {
    my $browser  = LWP::UserAgent->new;
    my $response = $browser->get("https://www.google.com");
    return $response->code eq '200';
}

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Utils::Network;

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;
