package App::ExhibitExplorer::Utils::Images;

use v5.34.0;
use strictures 2;

use Exporter qw( import );
use File::Fetch;
use File::Temp qw( tempdir );
use Image::Magick;
our @EXPORT_OK = qw( fetch_image );

=head1 NAME

App::ExhibitExplorer::Utils::Images - Utilities for dealing with images

=head1 VERSION

See L<App::ExhibitExplorer>

=cut

use App::ExhibitExplorer; our $VERSION = $App::ExhibitExplorer::VERSION;

=head1 SYNOPSIS

    use App::ExhibitExplorer::Utils::Images qw( fetch_image );
    eval {
        my $file = fetch_image('https://example.com/image.jpg');
        # do something with $file
        unlink($file);
        1;
    } or do {
        die "Oh no: $@";
    };

=head1 SUBROUTINES

=head2 fetch_image

    my $file = fetch_image('https://example.com/image.jpg');

Downloads the file at the URL to a local temporary file. They'll all be cleaned
up on program exit.  An exception will be raised if something goes wrong, even
if it's something like a 404.

=cut

sub fetch_image {
    my ($url) = @_;

    # To avoid files with the same name overwriting each other, use a random dir per each
    my $temp_dir = tempdir( CLEANUP => 1 );
    my $ff    = File::Fetch->new( uri => $url );
    my $where;
    my $retries = 10;
    while ( $retries && !$where ) {
        $where = $ff->fetch( to => $temp_dir );
        last if $where;
        $retries--;
        if ( $ff->error && $ff->error !~ /Temporary failure/i ) {

            # TODO fix my logging and log this
            die "Failed to fetch image '$url': " . $ff->error;
        }
        sleep 60;
    }
    if ( !$where ) {
        die "Failed to fetch image after retrying: " . $ff->error;
    }
    # If the image is huge, make it smaller
    my $img = Image::Magick->new();
    my $ierr = $img->Read($where);
    die "Error processing image: $ierr" if $ierr;
    my $width = $img->Get('width');
    my $height = $img->Get('height');
    if ($width > 2000 || $height > 2000) {
        $ierr = $img->Resize(geometry => "2000x2000");
        die "Error resizing image: $ierr" if $ierr;
        $ierr = $img->Write($where . "-resized");
        return $where . "-resized";
        # Thie results in an error, but still works, so we ignore the error :/
        #die "Error writing image: $ierr" if $ierr;
    }

    return $where;
}

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Utils::Images;

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;
