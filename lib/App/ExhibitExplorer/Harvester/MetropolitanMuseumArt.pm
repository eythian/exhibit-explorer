package App::ExhibitExplorer::Harvester::MetropolitanMuseumArt;

use v5.34.0;
use strictures 2;

use App::ExhibitExplorer::Utils::Images qw(fetch_image);
use Carp;
use DateTime::Format::ISO8601;
use Log::Log4perl::Level;
use Moo;
use Museum::MetropolitanMuseumArt;

with 'App::ExhibitExplorer::Harvester::Base';

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::MetropolitanMuseumArt - Ingests catalogue data from The Metropolitan Museum of Art

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

This is intended to be used by L<App::ExhibitExplorer::Harvester>.

    use App::ExhibitExplorer::Harvester::MetropolitanMuseumArt;

    my $harv = App::ExhibitExplorer::Harvester::MetropolitanMuseumArt->new(
        log => sub { ... }, # a sub for logging to
        config => $config,
        resultsset => $resultset, # DBIx::Class resultset to write to
        source_id => 2, # the DB ID of this source
    );
    my $status = $harv->harvest();
    my $item_details = $harv->get_item_details($record_id);

=head1 SUBROUTINES/METHODS

=head2 harvest

    my $status = $harv->harvest;

Begin ingesting the museum data and saving anything that's new.

=cut

sub harvest {
    my ($self) = @_;

    my $total_records  = 0;
    my $usable_records = 0;
    my $new_records    = 0;
    my $limited_run    = 0;     # mostly for debug/testing
    my $batch_size     = 200;

    my $client = Museum::MetropolitanMuseumArt->new();

    # Fetch all IDs from the API
    my $all_objects = $client->get_objects();
    if ($limited_run) {
        $all_objects = [ $all_objects->@[0 .. $limited_run-1] ];
    }
    $total_records = @$all_objects;

    # Iterate over them in batches of $batch_size
    my ( $start, $end ) = ( 0, $batch_size - 1 );
    while ( $start < @$all_objects ) {
        my $_end = $end > @$all_objects - 1 ? @$all_objects - 1 : $end;
        my ( $new, $usable ) = $self->_process_batch( $client, $all_objects->@[ $start .. $_end ] );
        $usable_records += $usable;
        $new_records    += $new;
        $start          += $batch_size;
        $end            += $batch_size;
    }
    $self->log->(
        $INFO,
"metropolitan: Fetched $total_records, $usable_records were usable, added $new_records new records"
    );
}

sub _process_batch {
    my ( $self, $client, @ids ) = @_;

    my $source_id = $self->source_id;

    # First see which of these IDs we already have
    # TODO look for old update times and re-check those in case new images appear
    my @existing = $self->resultset->search(
        {
            record_id => { -in => \@ids },
            source_id => $source_id,
        }
    );
    my %ids_hash;
    @ids_hash{@ids} = ();
    foreach my $e (@existing) {
        delete $ids_hash{ $e->record_id };
    }
    my @new_ids = sort { $a <=> $b } keys %ids_hash;
    my @records;
    my $fields = [qw( source_id record_id record_timestamp is_eligible ineligible_reason )];
    # We don't ratelimit here because we're only doing one request at a time
    # and that'll be slow enough
    my ($total, $eligible) = (0,0);
    foreach my $id (@new_ids) {
        eval {
            $total++;
            my $record      = $self->_get_item($client, $id);
            my ($is_eligible, $ineligible_reason) = (1, undef);
            if (!$record->{primaryImage} || $record->{primaryImage} eq '') {
                $is_eligible = 0;
                $ineligible_reason = 'No image';
            }
            $eligible++ if $is_eligible;
            push @records,
              [
                $source_id, $id,
                DateTime::Format::ISO8601->parse_datetime( $record->{metadataDate} ),
                $is_eligible, $ineligible_reason,
              ];
            1;
        } or do {
            my $err = 'error harvesting: ' . $@;
            $self->resultset->create(
                {
                    source_id     => $source_id,
                    record_id     => $id,
                    is_eligible   => 0,
                    error         => 1,
                    error_details => $err,
                }
            );
            $self->log->( $WARN, "metropolitan: Failed fetching id '$id': $err" );
        };
    }
    $self->resultset->populate( [ $fields, @records, ] );
    return ($total, $eligible);
}

sub _get_item {
    my ($self, $client, $id) = @_;

    my $record;
    my $retries = 20;
    my $success = 0;
    while (!$success) {
        eval {
            $record = $client->get_object($id);
            $success = 1;
            1;
        } or do {
            warn "error $@";
            my $error = $@;
            if ($error =~ /500/ && $retries) {
                $self->log( $WARN, "metropolitan: Item API returned 500" );
                $retries--;
                sleep 60;
            } else {
                die $error;
            }
        }
    }
    return $record;
}

=head2 get_item_details

    my $item_details = $harv->get_item_details($record_id);

Fetch the full details for a record. C<$record_id> should be the Metropolitan
Museum ID.  This also downloads the images. A hashref is returned with all the
useful information. An exception is thrown if there is an error.

=cut

sub get_item_details {
    my ( $self, $record_id ) = @_;

    $self->log->( $INFO, "metropolitan: Fetching record for '$record_id'" );
    my $client = Museum::MetropolitanMuseumArt->new();
    my $object = $self->_get_item( $client, $record_id );

    die "metropolitan: object id '$record_id' doesn't seem to have images\n"
      unless $object->{primaryImage};
    my @images = ( fetch_image( $object->{primaryImage} ) );
    if ( $object->{additionalImages} ) {
        foreach my $img ( $object->{additionalImages}->@* ) {
            push @images, fetch_image($img);
        }
    }

    my @description;
    push @description, $object->{objectName} if $object->{objectName};
    push @description, $object->{culture}    if $object->{culture};
    push @description, $object->{objectDate} if $object->{objectDate};
    my $description = join( ', ', @description );
    if ( $object->{artistDisplayName} ) {
        my $name = $object->{artistDisplayName};
        $name        .= ' (' . $object->{artistDisplayBio} . ')' if $object->{artistDisplayBio};
        $description .= ' by ' . $name;
    }

    my $title = $object->{title};

    # Prevents us repeating ourselves, e.g. "Plate\nPlate, 1923..."
    if ( lc($title) eq lc( $object->{objectName} ) ) {
        $title = $description;
        undef $description;
    }
    return {
        institution_name => 'Metropolitan Museum of Art',
        title            => $title,
        record_id        => $record_id,
        item_url         => $object->{objectURL},
        item_image_files => \@images,
        description      => $description,
        primary_author   => undef,                         # these are a little tricky so only worth
        date             => undef,                         # doing if we need them
    };
}

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Harvester::MetropolitanMuseumArt

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;

