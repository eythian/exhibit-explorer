package App::ExhibitExplorer::Harvester::Rijksmuseum;

use v5.34.0;
use strictures 2;

use App::ExhibitExplorer::Utils::Images qw(fetch_image);
use Carp;
use DateTime::Format::ISO8601;
use Log::Log4perl::Level;
use Moo;
use Museum::Rijksmuseum::Object;
use Museum::Rijksmuseum::Object::Harvester;

with 'App::ExhibitExplorer::Harvester::Base';

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::Rijksmusem - Ingests catalogue data from the Rijksmuseum

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

This is intended to be used by L<App::ExhibitExplorer::Harvester>.

    use App::ExhibitExplorer::Harvester::Rijksmuseum;

    my $harv = App::ExhibitExplorer::Harvester::Rijksmuseum->new(
        log => sub { ... }, # a sub for logging to
        config => $config,
        resultsset => $resultset, # DBIx::Class resultset to write to
        source_id => 2, # the DB ID of this source
    );
    my $status = $harv->harvest();
    my $item_details = $harv->get_item_details($record_id);

=head1 SUBROUTINES/METHODS

=head2 harvest

    my $status = $harv->harvest;

Begin ingesting the Rijksmuseum data and saving anything that's new.

=cut

sub harvest {
    my ($self) = @_;

    my $total_records = 0;
    my $new_records   = 0;
    my $limited_run   = 0;    # mostly for debug/testing
    my @batch;
    my $batch_size = 200;

    # This callback buffers records up to $batch_size, then sends them all off for
    # processing
    my $callback = sub {
        my ($rec) = @_;

        push @batch, $rec;
        if ( @batch >= $batch_size ) {
            $new_records += $self->_process_batch( \@batch );
            undef @batch;
        }

        $total_records++;
        if ( $limited_run && $total_records >= $limited_run ) {
            return 1;
        }
        return 0;
    };

    my $harv =
      Museum::Rijksmuseum::Object::Harvester->new( key => $self->config->{key} );
    my $status = $harv->harvest(
        callback => $callback,
        type     => 'identifiers',
        delay    => 0,
        set      => 'subject:PublicDomainImages',
    );
    $new_records += $self->_process_batch( \@batch );
    if ( $status->{error} ) {
        die "rijksmuseum: harvester reported an error: $status->{error}\n";
    }
    $self->log->( $INFO, "rijksmuseum: Fetched $total_records, added $new_records new records" );
}

sub _process_batch {
    my ( $self, $batch ) = @_;

    # First find out which of these are new
    my $source_id    = $self->source_id;
    my %id_to_record = map { $_->{identifier} => $_ } @$batch;
    my @existing     = $self->resultset->search(
        { record_id => { -in => [ keys %id_to_record ] }, source_id => $source_id } );
    foreach my $e (@existing) {
        delete $id_to_record{ $e->record_id };
    }
    return 0 unless %id_to_record;

    my $fields  = [qw( source_id record_id record_timestamp is_eligible )];
    my @records = map {
        [
            $source_id,                                                   $_->{identifier},
            DateTime::Format::ISO8601->parse_datetime( $_->{datestamp} ), 1,
        ]
    } values %id_to_record;
    $self->resultset->populate( [ $fields, @records, ] );
    return scalar(@records);
}

=head2 get_item_details

    my $item_details = $harv->get_item_details($record_id);

Fetch the full details for a record. C<$record_id> should be a full ID with OAI
namespacing.  This also downloads the image. A hashref is returned with all the
useful information. An exception is thrown if there is an error.

=cut

sub get_item_details {
    # Placeholder for testing
    my ( $self, $record_id ) = @_;

    # The record id is in OAI format, so let's parse that
    # https://www.openarchives.org/OAI/2.0/guidelines-oai-identifier.htm
    # We don't care about scheme and namespace here
    my (undef, undef, $local_id) = split(':', $record_id);
    croak "Unable to parse record id for Rijksmuseum: '$record_id'" unless $local_id;
    my $rijks = Museum::Rijksmuseum::Object->new(
        key => $self->config->{key},
        culture => $self->config->{culture},
    );
    my $object = $rijks->fetch($local_id);
    die "Failed to fetch item: $object->{error}\n" if $object->{error};

    my $art_object = $object->{artObject};
    die "No image file for rijksmuseum ID '$record_id'\n" unless $art_object->{webImage}{url};
    my $image_file = fetch_image($art_object->{webImage}{url});
    
    my $item_url_base = $self->config->{culture} eq 'en'
        ? 'https://www.rijksmuseum.nl/en/collection/'
        : 'https://www.rijksmuseum.nl/nl/collectie/';
    return {
        institution_name => 'Rijksmuseum',
        title            => $art_object->{longTitle} || $art_object->{title},
        record_id        => $record_id,
        item_url         => $item_url_base . $local_id,
        item_image_files => [$image_file],
        description      => $art_object->{description},
        primary_author   => $art_object->{principalMaker},
        date             => $art_object->{dating}{presentingDate},
    };
}

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Harvester::Rijksmuseum

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;
