package App::ExhibitExplorer::Harvester::Base;

use v5.34.0;
use strictures 2;

use Moo::Role;

=head1 NAME

App::ExhibitExplorer::Harvester::Base - base class for harvester modules

=head2 DESCRIPTION

This is the base class for harvester modules. It mostly holds all the standard
attributes that get passed by L<App::ExhibitExplorer::Harvester> when it
constructs objects.

=head1 METHODS

=cut

requires qw( harvest get_item_details );

=head1 ATTRIBUTES

=head2 log

The logger function.

=cut

has log => (
    is       => 'ro',
    required => 1,
);

=head2 config

This is the config structure for this module, typically direct from the C<config.toml>. It is particular to the specific module but generally contains API keys and such.

=cut

has config => (
    is       => 'ro',
    required => 1,
);

=head2 resultset

Probably wanting a rename, this is a resultset that can be used to access the
C<record> table for reading, adding and updating records.

=cut

has resultset => (
    is       => 'ro',
    required => 1,
);

=head2 source_id

The database ID of this source.

=cut

has source_id => (
    is       => 'ro',
    required => 1,
);

=head2 subsources

An array of result objects that contain the subsource information that applies
to this module.

=cut

has subsources => (
    is       => 'ro',
    required => 0,
);

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Harvester::Base

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back

=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2024 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;

