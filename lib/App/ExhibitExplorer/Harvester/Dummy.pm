package App::ExhibitExplorer::Harvester::Dummy;

use v5.34.0;
use strict;
use warnings;

use DateTime::Format::ISO8601;
use Log::Log4perl::Level;
use Moo;

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::Dummy - A dummy harvest provider, used for testing and debugging

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

This is intended to be used by L<App::ExhibitExplorer::Harvester>.

    use App::ExhibitExplorer::Harvester::Dummy;

    my $harv = App::ExhibitExplorer::Harvester::Dummy->new(
        log => sub { ... }, # a sub for logging to
        config => $config,
        resultsset => $resultset, # DBIx::Class resultset to write to
        source_id => 1, # the DB ID of this source
    );
    my $status = $harv->harvest();
    my $item_details = $harv->get_item_details($record_id);

=head1 SUBROUTINES/METHODS

=head2 harvest

    my $status = $harv->harvest;

Begin fake-ingesting data and writing it into the database. It always marks things
as ineligible for use.

=cut

sub harvest {
    my ($self) = @_;

    # Creates three entries with IDs qw( d1 d2 d3 ), and walks through all the
    # usual steps to check if they need to be created etc.

    # Pretend we've done an API call to get these
    my $results = [
        { id => 'd1', description => 'Dummy item 1', timestamp => '2023-01-01T01:00:00' },
        { id => 'd2', description => 'Dummy item 2', timestamp => '2023-01-01T04:00:00' },
        { id => 'd3', description => 'Dummy item 3', timestamp => '2023-01-01T07:00:00' },
    ];
    my %id_to_record  = map { $_->{id} => $_ } @$results;
    my $total_records = keys %id_to_record;
    my @existing = $self->resultset->search(
        { record_id => { -in => [ keys %id_to_record ] }, source_id => $self->source_id } );
    
    # Filter out ones already in the database
    foreach my $e (@existing) {
        delete $id_to_record{$e->record_id};
    }

    if (%id_to_record) {
        while ( my ( $id, $record ) = each(%id_to_record) ) {
            $self->resultset->create(
                {
                    source_id        => $self->source_id,
                    record_id        => $record->{id},
                    record_timestamp =>
                      DateTime::Format::ISO8601->parse_datetime( $record->{timestamp} ),
                    description       => $record->{description},
                    is_eligible       => 0,
                    ineligible_reason => 'Dummy record',
                }
            );
        }
    }
    my $new_records = keys %id_to_record;
    $self->log->( $INFO, "dummy: Fetched $total_records, added $new_records new records" );
}

sub get_item_details {
    my ( $self, $record_id ) = @_;

    return {
        institution_name => 'Dummy Museum',
        record_id        => $record_id,
        item_url         => "https://example.com/dummymuseum/$record_id",
        item_image_file  => undef,
        description      => "This is dummy item $record_id",
    };
}

has log => (
    is       => 'ro',
    required => 1,
);

has config => (
    is       => 'ro',
    required => 1,
);

has resultset => (
    is       => 'ro',
    required => 1,
);

has source_id => (
    is       => 'ro',
    required => 1,
);

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Harvester::Dummy

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;
