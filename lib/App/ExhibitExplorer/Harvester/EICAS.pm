package App::ExhibitExplorer::Harvester::EICAS;

use v5.34.0;
use strictures 2;

use App::ExhibitExplorer::Utils::Images qw(fetch_image);
use Carp;
use DateTime::Format::ISO8601;
use Encode qw( encode );
use Log::Log4perl::Level;
use Moo;
use Museum::EICAS;

with 'App::ExhibitExplorer::Harvester::Base';

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::Harvester::EICAS - Ingests catalogue data from EICAS

=cut

sub harvest {
    my ($self) = @_;

    my $total_records  = 0;
    my $usable_records = 0;
    my $new_records    = 0;

    my $client = Museum::EICAS->new();

    my $all_items = $client->get_items();
    $total_records = @$all_items;

    # Find all the usable records
    my @usable_items = grep { $_->{'o:media'} && $_->{'o:media'}->@* } @$all_items;
    $usable_records = @usable_items;
    my %usable_items = map { $_->{'o:id'} => $_ } @usable_items;

    # Extract all the IDs and see what we already have in the DB
    my $source_id = $self->source_id;
    my @ids       = map { $_->{'o:id'} } @usable_items;
    my @existing  = $self->resultset->search(
        {
            record_id => { -in => \@ids },
            source_id => $source_id,
        }
    );

    my %ids_hash;
    @ids_hash{@ids} = ();
    foreach my $e (@existing) {
        delete $ids_hash{ $e->record_id };
    }
    my @new_ids = sort { $a <=> $b } keys %ids_hash;
    $new_records = @new_ids;
    my $fields  = [qw( source_id record_id record_timestamp is_eligible )];
    my @records = map {
        [
            $source_id,                                                                $_->{'o:id'},
            DateTime::Format::ISO8601->parse_datetime( $_->{'o:modified'}{'@value'} ), 1,
        ]
    } @usable_items{@new_ids};

    $self->resultset->populate( [ $fields, @records, ] );
    $self->log->(
        $INFO,
        "eicas: Fetched $total_records, $usable_records were usable, added $new_records new records"
    );
}

sub get_item_details {
    my ( $self, $record_id ) = @_;

    $self->log->( $INFO, "eicas: Fetching record for '$record_id'" );
    my $client = Museum::EICAS->new();
    my $item   = $client->get_item($record_id);
    my $media  = $client->get_media($item);

    die "eicas: item id '$record_id' seems to have no media\n" unless ( $media && @$media );

    my @images;
    foreach my $m (@$media) {
        push @images, fetch_image( $m->{'o:original_url'} );
    }

    my $title = encode('UTF-8', $item->{'o:title'});

    my @description;
    push @description, $item->{'foaf:theme'}[0]{'@value'}
      if $item->{'foaf:theme'}[0]{'@value'};
    push @description, $item->{'dcterms:description'}[0]{'@value'}
      if $item->{'dcterms:description'}[0]{'@value'};
    push @description, map { $_->{'@value'} }
      grep { $_->{'@value'} && $_->{type} eq 'literal' } $item->{'dcterms:creator'}->@*;
    my $description = encode('UTF-8', join( "\n\n", @description ));

    my $url = 'https://collectie.eicas.nl/item/' . $record_id;

    return {
        institution_name => 'EICAS',
        title            => $title,
        record_id        => $record_id,
        item_url         => $url,
        item_image_files => \@images,
        description      => $description,
        primary_author   => undef,          # these are a little tricky so only worth
        date             => undef,          # doing if we need them
    };
}

1;
