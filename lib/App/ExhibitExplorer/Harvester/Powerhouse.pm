package App::ExhibitExplorer::Harvester::Powerhouse;

use v5.34.0;
use strictures 2;

use App::ExhibitExplorer::Utils::Images qw(fetch_image);
use Carp;
use GraphQL::Client;
use Log::Log4perl::Level;
use Moo;

with 'App::ExhibitExplorer::Harvester::Base';

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::Powerhouse - Ingests catalogue data from the Powerhouse (MAAS) museum

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

This is intended to be used by L<App::ExhibitExplorer::Harvester>.

    use App::ExhibitExplorer::Harvester::Powerhouse;

    my $harv = App::ExhibitExplorer::Harvester::Powerhouse->new(
        log => sub { ... }, # a sub for logging to
        config => $config,
        resultsset => $resultset, # DBIx::Class resultset to write to
        source_id => 5, # the DB ID of this source
    );
    my $status = $harv->harvest();
    my $item_details = $harv->get_item_details($record_id);

This uses the GraphQL API described at L<https://api.maas.museum/docs/graphql>.

=head1 SUBROUTINES/METHODS

=head2 harvest

    my $status = $harv->harvest;

Begin ingesting the Powerhouse data and saving anything that's new.

=cut

sub harvest {
    my ($self) = @_;

    my $total_records = 0;
    my $new_records   = 0;
    my $limited_run   = 0;
    my $batch_size    = 200;

    my $objects;
    my $bailout = 0;
    my $skip   = 0;
    my $client = GraphQL::Client->new( url => 'https://api.maas.museum/graphql', unpack => 1 );
    do {
        my $query = qq[{
            objects(limit: $batch_size, skip: $skip, filter: {hasMedia: true}) {
                _id
                displayTitle
                description
                statement
                lastUpdated
                images {
                  url
                }
            }
        }];
        my $data = $client->execute( $query );
        $objects = $data->{objects};
        $total_records += @$objects;
        $skip += @$objects;
        $bailout = 1 if ($limited_run && ($total_records >= $limited_run));
        $new_records += $self->_process_batch($objects) if @$objects;
    } while ( @$objects && !$bailout );
    $self->log->( $INFO, "powerhouse: Fetched $total_records, added $new_records new records" );
}

sub _process_batch {
    my ($self, $batch) = @_;

    my $source_id    = $self->source_id;

    # Filter batch to ensure we have images
    $batch = [ grep { $_->{images} && $_->{images}->@* > 0 } @$batch ];
    my %id_to_record = map { $_->{_id} => $_ } @$batch;
    my @existing     = $self->resultset->search(
        { record_id => { -in => [ keys %id_to_record ] }, source_id => $source_id } );
    foreach my $e (@existing) {
        delete $id_to_record{ $e->record_id };
    }
    return 0 unless %id_to_record;
    
    my $fields  = [qw( source_id record_id record_timestamp is_eligible )];
    my @records = map {
        [
            $source_id, $_->{_id},
            _parse_stupid_timestamp( $_->{lastUpdated} ), 1,
        ]
    } values %id_to_record;
    $self->resultset->populate( [ $fields, @records, ] );
    return scalar(@records);
}

=head2 get_item_details

    my $item_details = $harv->get_item_details($record_id);

Fetch the full details for a record. C<$record_id> should be the '_id' returned
by the API.  This also downloads the image. A hashref is returned with all the
useful information. An exception is thrown if there is an error.

=cut

sub get_item_details {
    my ($self, $record_id) = @_;

    my $client = GraphQL::Client->new( url => 'https://api.maas.museum/graphql', unpack => 1 );
    my $query = qq[{
        object(filter: {_id:$record_id}) {
            _id
            displayTitle
            description
            statement
            lastUpdated
            images {
              url
            }
        }
    }];
    my $data = $client->execute( $query );
    my $object = $data->{object};
    die "Got no object data for powerhouse id '$record_id'\n" unless $object && %$object && $object->{_id};
    my $images = $object->{images};
    die "No image files for powerhouse id '$record_id'\n" unless $images && @$images > 0;
    my @images = map { fetch_image($_->{url}) } @$images;
    my $description = $object->{description};
    my $url = 'https://ma.as/' . $object->{_id};
    return {
        institution_name => 'Powerhouse',
        title            => $object->{displayTitle},
        record_id        => $record_id,
        item_url         => $url,
        item_image_files => \@images,
        description      => $description,
        primary_author   => undef,
        date             => undef,
    };
}

# Handle:
# Thu Jun 22 2023 14:00:00 GMT+0000 (Coordinated Universal Time)
my $strp = DateTime::Format::Strptime->new(
    pattern => '%B %d %Y %T %Z%z',
    locale  => 'en_AU',
    on_error => 'croak',
    strict => 1,
);

sub _parse_stupid_timestamp {
    my ($stamp) = @_;

    # strptime understands 'Sept' but not 'Sep', etc.
    $stamp =~ s/sep/sept/i;
    $stamp =~ s/jun/june/i;
    $stamp =~ s/jul/july/i;

    my $dt = $strp->parse_datetime($stamp);    
    return $dt;
}

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Harvester::Powerhouse

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;
