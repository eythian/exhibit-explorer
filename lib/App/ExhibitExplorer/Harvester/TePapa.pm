package App::ExhibitExplorer::Harvester::TePapa;

use v5.34.0;
use strictures 2;

use App::ExhibitExplorer::Utils::Images qw(fetch_image);
use Carp;
use DateTime::Format::ISO8601;
use HTML::Strip;
use Log::Log4perl::Level;
use Moo;
use Museum::TePapa;

with 'App::ExhibitExplorer::Harvester::Base';

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::TePapa - Ingests catalogue data from Te Papa

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

This is intended to be used by L<App::ExhibitExplorer::Harvester>.

    use App::ExhibitExplorer::Harvester::TePapa;

    my $harv = App::ExhibitExplorer::Harvester::TePapa->new(
        log => sub { ... }, # a sub for logging to
        config => $config,
        resultsset => $resultset, # DBIx::Class resultset to write to
        source_id => 2, # the DB ID of this source
    );
    my $status = $harv->harvest();
    my $item_details = $harv->get_item_details($record_id);

=head1 SUBROUTINES/METHODS

=head2 harvest

    my $status = $harv->harvest;

Begin ingesting the Te Papa data and saving anything that's new.

=cut

sub harvest {
    my ($self) = @_;

    my $total_records = 0;
    my $usable_records = 0;
    my $new_records   = 0;
    my $limited_run   = 0;    # mostly for debug/testing
    my @batch;
    my $batch_size = 200;

    my $callback = sub {
        my ($records) = @_;
            foreach my $rec ($records->{results}->@*) {
            $total_records++;
            next unless $self->_is_usable($rec);
            $usable_records++;
            push @batch, $self->_extract_fields($rec);
            if ( @batch >= $batch_size ) {
                $new_records += $self->_process_batch( \@batch );
                undef @batch;
            }

            if ( $limited_run && $total_records >= $limited_run ) {
                return 1;
            }
        }
        return 0;
    };

    my $harv = Museum::TePapa->new( key => $self->config->{key} );
    my $status = $harv->object_scroll($callback);
    $new_records += $self->_process_batch( \@batch );
    $self->log->( $INFO, "tepapa: Fetched $total_records, $usable_records were usable, added $new_records new records" );
}

sub _is_usable {
    my ($self, $record) = @_;

    # Do we have an image?
    return 0 unless $record->{hasRepresentation} && $record->{hasRepresentation}->@*;
    my $has_image = 0;
    foreach my $rep ($record->{hasRepresentation}->@*) {
        if ($rep->{type} eq 'ImageObject' && $rep->{rights}{allowsDownload}) {
            $has_image = 1;
            last;
        }
    }
    return 0 unless $has_image;
    return 1;
}

sub _extract_fields {
    my ( $self, $record ) = @_;

    return {
        identifier => $record->{id},
        datestamp  => $record->{_meta}{modified},
    };
}

sub _process_batch {
    my ( $self, $batch ) = @_;

    # First find out which of these are new
    my $source_id    = $self->source_id;
    my %id_to_record = map { $_->{identifier} => $_ } @$batch;
    my @existing     = $self->resultset->search(
        { record_id => { -in => [ keys %id_to_record ] }, source_id => $source_id } );
    foreach my $e (@existing) {
        delete $id_to_record{ $e->record_id };
    }
    return 0 unless %id_to_record;

    my $fields  = [qw( source_id record_id record_timestamp is_eligible )];
    my @records = map {
        [
            $source_id,                                                   $_->{identifier},
            DateTime::Format::ISO8601->parse_datetime( $_->{datestamp} ), 1,
        ]
    } values %id_to_record;
    $self->resultset->populate( [ $fields, @records, ] );
    return scalar(@records);
}

=head2 get_item_details

    my $item_details = $harv->get_item_details($record_id);

Fetch the full details for a record. C<$record_id> should be the Te Papa ID.
This also downloads the image. A hashref is returned with all the useful
information. An exception is thrown if there is an error.

=cut

sub get_item_details {
    my ( $self, $record_id ) = @_;

    $self->log->($INFO, "tepapa: Fetching record for '$record_id'");
    my $tp = Museum::TePapa->new(
        key => $self->config->{key},
    );
    my $object = $tp->object($record_id);

    # Get the images we can use
    die "tepapa: object id '$record_id' doesn't seem to have images\n" unless $object->{hasRepresentation};
    my @images;
    foreach my $rep ($object->{hasRepresentation}->@*) {
        next unless $rep->{type} eq 'ImageObject' && $rep->{rights}{allowsDownload};
        my $image_file = fetch_image($rep->{contentUrl});
        push @images, $image_file;
    }

    die "No image files for tepapa ID '$record_id'\n" unless @images;
    my $item_url_base = 'https://collections.tepapa.govt.nz/object/';
    return {
        institution_name => 'Te Papa Tongarewa',
        title            => $object->{caption} || $object->{title},
        record_id        => $record_id,
        item_url         => $item_url_base . $record_id,
        item_image_files => \@images,
        description      => $self->_cleanup( $object->{description} ),
        primary_author   => undef,    # these are a little tricky so only worth
        date             => undef,    # doing if we need them
    };
}

sub _cleanup {
    my ($self, $str) = @_;

    return undef unless defined $str;
    # Remove HTML guff
    # but first, make <br> linebreaks
    $str =~ s|<br */?>|\n|ig;
    $str =~ s|&nbsp;| |ig; # and clean these
    my $hs = $self->{htmlstrip};
    if (!$hs) {
        $hs = HTML::Strip->new();
        $self->{htmlstrip} = $hs;
    }
    my $out = $hs->parse($str);
    $hs->eof;
    return $out;
}

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Harvester::Rijksmuseum

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;
