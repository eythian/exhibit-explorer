package App::ExhibitExplorer::Harvester::DigitalNZ;

use v5.34.0;
use strictures 2;

use App::ExhibitExplorer::Utils::Images qw(fetch_image);
use Carp;
use Encode qw( encode decode );
use Log::Log4perl::Level;
use Moo;
use Net::DigitalNZ::V3;

with 'App::ExhibitExplorer::Harvester::Base';

use namespace::clean;

# This is the module for supporting DigitalNZ. It works differently than the
# other modules, as DNZ has many partners, which I'll call subsources.

# Regarding ratelimiting: DigitalNZ has a max API calls per day of 10,000 and
# and max results per query of 100. This makes a max of 1 million records queried
# per day. Should we go above that, it's something to work out then.

sub harvest {
    my ($self) = @_;

    # Get the list of subsources we are going to work with. The names must match what
    # DigitalNZ calls content_partner in the API
    my @subsource_names;
    my $subs = $self->subsources;
    my %subname_to_id;
    foreach my $ss (@$subs) {
        push @subsource_names, $ss->name;
        $subname_to_id{ $ss->name } = $ss->id;
    }
    my $content_partners = [ map { +"content_partner" => decode('UTF-8', $_) } @subsource_names ];
    my $filters          = {
        -and => [
            usage                   => 'Share',
            category                => 'Images',
            has_large_thumbnail_url => 'Y',        # weird, but documented in swagger
        ],
        -or => $content_partners,
    };

    my $total_records = 0;
    my $new_records   = 0;
    my $limited_run   = 0;
    my $page_size     = 100;
    my $page          = 1;

    my $done = 0;
    my $dnz  = Net::DigitalNZ::V3->new(
        api_key   => $self->config->{key},
        useragent => 'ExhibitExplorer / https://botsin.space/@ExhibitExplorer'
    );
    while ( !$done ) {
        my $result = $dnz->search(
            filters  => $filters,
            per_page => $page_size,
            page     => $page,
            fields   => 'id,content_partner,updated_at,title,syndication_date',

            # not all records have syndication_date, but providing it seems to
            # make the search results ordering stable. Sort by 'id' returns a 500.
            sort => 'syndication_date',
        );
        $page++;
        $total_records += $result->{search}{results}->@*;
        $new_records   += $self->_process_batch( \%subname_to_id, $result );
        if ( $result->{search}{results}->@* == 0
            || ( $limited_run > 0 && $total_records >= $limited_run ) )
        {
            $done = 1;
        }
    }
    $self->log->( $INFO, "digitalnz: Fetched $total_records, added $new_records new records" );
}

sub _process_batch {
    my ( $self, $subname_to_id, $result ) = @_;
    my $total = 0;

    # First see which of these IDs we already have
    my $source_id = $self->source_id;
    my @ids       = map { $_->{id} } $result->{search}{results}->@*;
    my @existing  = $self->resultset->search(
        {
            record_id => { -in => \@ids },
            source_id => $source_id,
        }
    );
    my %existing;
    $existing{ $_->record_id } = 1 foreach (@existing);
    my @to_store = grep { !$existing{ $_->{id} } } $result->{search}{results}->@*;
    my $fields   = [qw( source_id subsource_id record_id record_timestamp is_eligible )];
    my @records;

    foreach my $r (@to_store) {
        my $ss_id = $subname_to_id->{ encode('UTF-8', $r->{content_partner}[0]) };
        die "Unknown content partner in record ID "
          . $r->{id} . ", '"
          . $r->{content_partner} . "'\n"
          unless $ss_id;
        push @records,
          [
            $source_id, $ss_id, $r->{id},
            DateTime::Format::ISO8601->parse_datetime( $r->{updated_at} ), 1,
          ];
        $total++;
    }
    $self->resultset->populate( [ $fields, @records ] );

    return $total;
}

sub get_item_details {
    my ( $self, $record_id ) = @_;

    $self->log->( $INFO, "digitalnz: Fetching record for '$record_id'" );
    my $dnz = Net::DigitalNZ::V3->new(
        api_key   => $self->config->{key},
        useragent => 'ExhibitExplorer / https://botsin.space/@ExhibitExplorer'
    );
    my $record = $dnz->metadata(
        record_id => $record_id,
        fields    =>
          'title,description,display_content_partner,large_thumbnail_url,source_url,attachments,display_date',
    );
    my $detail = $record->{record};

    # Gather up images. We're going to be sneaky here: digitalnz sometimes
    # gives us a URL for large_thumbnail, but there is also an xlarge which we
    # can get to by messing with the URL. So we'll do that and use the URL
    # we're given as a fallback.
    my @images_with_fallback;
    for my $att ( $detail->{attachments}->@* ) {
        my $i = $att->{url} || $att->{large_thumbnail_url};
        next unless $i;
        my @imgs;
        if ( $i =~ m{/large/} ) {
            push @imgs, $i =~ s{/large/}{/xlarge}r;
        }
        push @imgs,                 $i;
        push @images_with_fallback, \@imgs;
    }
    # If there are no attachments, fall back to the record-level thumbnail
    if ( !@images_with_fallback && $detail->{large_thumbnail_url} ) {
        my $i = $detail->{large_thumbnail_url};
        my @imgs;
        if ( $i =~ m{/large/} ) {
            push @imgs, $i =~ s{/large/}{/xlarge}r;
        }
        push @imgs,                 $i;
        push @images_with_fallback, \@imgs;
    }
    die "digitalnz: No images found for DigitalNZ ID '$record_id'\n" unless @images_with_fallback;

    my @image_files;
    foreach my $fb_img (@images_with_fallback) {
        my $works;
        foreach my $img (@$fb_img) {
            # ignore errors mostly
            eval { 
                $works = fetch_image($img); 
                if ($works) {
                    break;
                }
            };
        }
        if ( !$works ) {
            die "digitalnz: failed to fetch image for DigitalNZ ID '$record_id': $@\n";
        }
        push @image_files, $works;
    }

    return {
        institution_name => 'DigitalNZ',    # we don't really use this
        title            => encode( 'UTF-8', $detail->{title} ),
        description      => encode( 'UTF-8', $detail->{description} ),
        item_url         => $detail->{source_url},
        item_image_files => \@image_files,
        record_id        => $record_id,
        display_date     => $detail->{display_date},
        credit => encode( 'UTF-8', $detail->{display_content_partner} . ' via DigitalNZ' ),
    };
}

1;
