package App::ExhibitExplorer::Publisher::Console;

use v5.34.0;
use strictures 2;

use Carp;
use Data::Printer;
use File::Which qw( which );
use Log::Log4perl::Level;
use Moo;

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::Publisher::Console - a simple publisher that outputs
directly to the console, mostly for testing.

=head1 VERSION

See L<App::ExhibitExplorer>

=cut

use App::ExhibitExplorer; our $VERSION = $App::ExhibitExplorer::VERSION;

=head1 SYNOPSIS

    use App::ExhibitExplorer::Publisher::Console;

    my $pub = App::ExhibitExplorer::Publisher::Console->new(
        config => $config,
        log => $logger,
    );
    $pub->publish($item_details);

=head1 SUBROUTINES/METHODS

=head2 new

    my $pub = App::ExhibitExplorer::Publisher::Console->new(
        config => $config,
        log => $logger,
    );

Instantiate the publisher. C<config> contains any config this module requires, which isn't
anything. C<log> is a sub reference to log to.

=head2 publish

    $pub->publish($item_details);

Publishes the item using the information provided by the relevant harvester
object. Raises an exception in the weird situation that something fails.

=cut

sub publish {
    my ($self, $item) = @_;

    my $ascii_conv = which('ascii-image-converter');
    unless ($ascii_conv) {
        print "If you had ascii-image-converter installed, you'd see an image here.\n";
    }
    if ($ascii_conv && $ascii_conv =~ /snap/) {
        print "ascii-image-converter won't work for us if installed as snap due to sandboxing.\n";
        undef $ascii_conv;
    }

    if ($ascii_conv) {
        system($ascii_conv, '-c', '-C', $_) foreach $item->{item_image_files}->@*;
    }
    p $item;
}

has log => (
    is       => 'ro',
    required => 1,
);

has config => (
    is       => 'ro',
    required => 1,
);

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Publisher::Console

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;
