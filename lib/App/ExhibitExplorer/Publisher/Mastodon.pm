package App::ExhibitExplorer::Publisher::Mastodon;

use v5.34.0;
use strictures 2;

use Carp;
use Encode qw( decode );
use Log::Log4perl::Level;
use Moo;
use Mastodon::Client;

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::Publisher::Mastodon - a publisher that will send entries
to a configured Mastodon account.

=head1 VERSION

See L<App::ExhibitExplorer>

=cut

use App::ExhibitExplorer; our $VERSION = $App::ExhibitExplorer::VERSION;

=head1 SYNOPSIS

    use App::ExhibitExplorer::Publisher::Mastodon;

    my $pub = App::ExhibitExplorer::Publisher::Mastodon->new(
        config => $config,
        log => $logger,
    );
    $pub->publish($item_details);

=head1 SUBROUTINES/METHODS

=head2 new

Instantiate the Mastodon publisher. C<config> contains the config needed, the application's TOML should have a section like:

    [publisher.mastodon]
    enabled = 1
    instance = "mastodon.server"
    client_id = "<client ID"
    client_secret = "<client secret"
    access_token = "<account access token>"

See C<bin/registermastodon> for information on generating these.

=head2 publish

    $publ->publish($item_details);

Publishes the item. Raises an exception if something fails.

=cut

sub publish {
    my ( $self, $item ) = @_;

    my $mc;
    my ($retries, $retriable) = (5, 1);
    my $error;
    while ($retries && $retriable) {
        eval {
            undef $error;
            my $config = $self->config;
            $mc = Mastodon::Client->new(
                instance        => $config->{instance},
                client_id       => $config->{client_id},
                client_secret   => $config->{client_secret},
                access_token    => $config->{access_token},
                coerce_entities => 1,
            );

            my @image_files = $item->{item_image_files}->@*;
            $#image_files = 3 if @image_files > 4;
            my @media = map { $mc->upload_media($_) } @image_files;

            my $title       = $item->{title};
            my $description = $item->{description} // '';

            # If the date is not already in the content, we add it to the title
            my $display_date = $item->{display_date};
            if (   $display_date
                && index( $title,       $display_date ) == -1
                && index( $description, $display_date ) == -1 )
            {
                $title .= ", $display_date";
            }

            # Form the message content. Deliberatly look at the original title, not
            # the one with date added.
            my $message = $title;
            if ( $description && $description ne $item->{title} ) {
                $message .= "\n\n" . $description;
            }

            # 500 char limit - 2 newlines - url length (fixed at 23 by mastodon)
            # the credit line is always fully included
            my $credit_line = $item->{credit} ? "\n" . $item->{credit} . "\n" : '';
            my $message_max_size = $config->{message_max_size} || 500;
            $message =
              shorten( $message, $message_max_size - 2 - 23 - length($credit_line) );
            if ($credit_line) {
                $message .= $credit_line;
            }
            $message .= "\n\n" . $item->{item_url};
            # This fixes mojibake, I guess something in the pipeline wantonly encodes to UTF-8
            # without seeing what's already there.
            $message = decode('UTF-8', $message);
            my $status = $mc->post_status( $message, { media_ids => [ map { $_->id } @media ] } );

            $self->log->(
                $INFO,
    "Posted item from $item->{institution_name} ($item->{record_id}) to Mastodon as status id '"
                  . $status->id . "'"
            );

            $retriable = 0;
            1;
        } or do {
            my $e = $@;
            if ($mc) {
                $error = "Failure publishing to Mastodon: $e, " . $mc->latest_response->{_content};
            } else {
                $error = "Failure instantiating Mastodon client: $e";
            }
            if ($mc && $mc->latest_response->code >= 500 && --$retries) {
                $self->log->($WARN, "Temporary failure or server issue publishing to Mastodon, retrying: $e, " . $mc->latest_response->{_content});
                sleep 60;
            } else {
                $retriable = 0;
            }
        };
    }
    die $error if $error;
}

sub shorten {
    my ( $str, $len ) = @_;

    return $str if length($str) <= $len;
    return substr( $str, 0, $len - 3 ) . '...';
}

has log => (
    is       => 'ro',
    required => 1,
);

has config => (
    is       => 'ro',
    required => 1,
);

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Publisher::Mastodon

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;

