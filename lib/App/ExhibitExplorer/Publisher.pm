package App::ExhibitExplorer::Publisher;

use v5.34.0;
use strictures 2;

use Carp;
use DateTime;
use JSON::MaybeXS;
use Log::Log4perl;
use Log::Log4perl::Level;
use Moo;

use DBIx::Class::SQLMaker::MySQL; # Loaded for monkey-patching

use App::ExhibitExplorer::Schema;
use App::ExhibitExplorer::SourceFactory;

use namespace::clean;


=head1 NAME

App::ExhibitExplorer::Publisher - Manages the publication of items for Exhibit Explorer

=head1 VERSION

See L<App::ExhibitExplorer>

=cut

use App::ExhibitExplorer; our $VERSION = $App::ExhibitExplorer::VERSION;

=head1 SYNOPSIS

This selects a random eligible entry from the database, fetches the full data from
the source, and sends that out to any defined publishers.

    use App::ExhibitExplorer::Publisher;

    my $pub = App::ExhibitExplorer::Publisher->new(config => $config);
    my $status = $pub->publish();
    if ($status->{error}) {
        die "Something failed: $status->{error}\n";
    }

=head1 CONFIGURATION

This takes the same configuration format as L<App::ExhibitExplorer::Harvester>, though
ignores the C<source> parts, and does care about the C<publisher> parts.

=head1 SUBROUTINES/METHODS

=head2 new

    my $pub = App::ExhibitExplorer::Publisher->new(config => $config);

Instantiate a new instance of the publisher, with its configuration.

=cut

sub BUILD {
    my ( $self, $args ) = @_;
    Log::Log4perl->easy_init($DEBUG);
    $self->{_logger} = Log::Log4perl->get_logger();
    my $db = $args->{config}{database};
    croak "Database configuration missing" unless $db;
    $self->{schema} =
      App::ExhibitExplorer::Schema->connect( 'dbi:' . $db->{type} . ':dbname=' . $db->{database},
        $db->{username}, $db->{password} );
    $self->{sourcefactory} = App::ExhibitExplorer::SourceFactory->new( schema => $self->{schema} );

    # I want debias to be on by default
    $args->{config}{options}{debias} = 1 unless exists $args->{config}{options}{debias};
}

=head2 publish

    my $status = $pub->publish( publishers => [qw(console)], testing => 1 );

Start running the publisher. If C<publishers> is provided, only those ones are
considered enabled, regardless of what the config says.

If C<testing> is set, then the database isn't updated to say that these items have
been previously published.

Throws an exception if something went wrong.

=cut

sub publish {
    my ($self, %args) = @_;

    my $exception;
    eval {
        my $item;
        my $item_details;
        my $retries = 20;
        my $backoff_delay = 1;
        my $item_error;
        do {
            $item = $self->_find_item($args{sources}, $args{subsources});

            # This indicates a database with no eligible items or similar
            unless ($item) {
                my $message = "Unable to find an item to publish";
                $self->_log($ERROR, $message);
                die "$message\n";
            }

            undef $item_error;
            eval {
                $item_details = $self->_get_item_details($item);
                1;
            } or do {
                $item_error = $@ || 'Zombie error';
            };

            if ($item_error) {
                if ($retries-- > 0) {
                    $self->_log($ERROR, "Failed to fetch details for item id '".$item->id."' (source id '".$item->source_id."'), $retries tries remaining: " . $item_error);
                    $self->_mark_error($item, $item_error);
                    sleep($backoff_delay);
                    $backoff_delay *= 1.5;
                } else {
                    my $message = "Failed to fetch an item, retries exhausted, bailing out";
                    $self->_log($ERROR, $message);
                    die "$message\n";
                }
            }
        } while ($item_error);
        
        # Make a list of publish destinations
        # But first, handle us being given specific ones to use
        if ($args{publishers}) {
            # Make all config entries not enabled
            foreach my $p (keys $self->config->{publisher}->%*) {
                $self->config->{publisher}{$p}{enabled} = 0;
            }
            # Make all given to us enabled
            foreach my $p ($args{publishers}->@*) {
                $self->config->{publisher}{$p}{enabled} = 1;
            }
        }
        
        my @publishers;
        foreach my $p (keys $self->config->{publisher}->%*) {
            push @publishers, $p if $self->config->{publisher}{$p}{enabled};
        }

        # Actually do the publishing now
        $self->_log( $INFO,
                "Starting publishing item id '$item_details->{record_id}' (DB id '"
              . $item->id
              . "', source '"
              . $item->source_id
              . "') with publishers "
              . join( ', ', @publishers ) );
        my %publish_failures;
        foreach my $publisher (@publishers) {
            $publish_failures{$publisher} = 0;
            eval {
                my $pub = $self->{sourcefactory}->get_publisher_for_name(
                    $publisher,
                    config => $self->config->{publisher},
                    log    => sub { $self->_log(@_); },
                );
                $pub->publish($item_details);
                1;
            } or do {
                my $error = $@ || 'Zombie error';
                $self->_log( $ERROR, "exception publishing '$publisher': $@" );
                $publish_failures{$publisher} = 1;
            };
        }
        # If there was a success, we mark this as sent, otherwise we don't
        my $successes = grep { !$_ } values %publish_failures;
        if ( $successes && !$self->testing ) {
            $self->_mark_sent($item);
        } else {
            $self->_mark_unsent($item);
        }

        $self->_log($INFO, "Done publishing.");

        1;
    } or do {
        my $error = $@ || 'Zombie error';
        $self->_log( $ERROR, "publish() had an exception: $@" );
        $exception = $error;
    };
    $self->_close_log;
    die $exception if $exception;
}

# We are monkey-patching DBIx::Class here as the MySQL driver is missing the
# understanding of scalars in 'for'
# https://rt.cpan.org/Ticket/Display.html?id=146996&results=f6f1a858effa189e87815abd67c97824
no warnings 'redefine';
sub DBIx::Class::SQLMaker::MySQL::_lock_select {
    my ( $self, $type ) = @_;

    my $sql;
    if ( ref($type) eq 'SCALAR' ) {
        $sql = "FOR $$type";
    } else {
        no warnings 'once';
        $sql = $DBIx::Class::SQLMaker::MySQL::for_syntax->{$type}
          || $self->throw_exception("Unknown SELECT .. FOR type '$type' requested");
    }

    return " $sql";
}

# Finds a random item from the database.
# As a side-effect, this sets is_used to 1 within the locked transaction. It
# does not set the used_timestamp. If sending later fails, set the is_used back
# to 0. is_used being 1 without used_timestamp being set can be used as a
# "bug in the code" type indication.
# An array of source names can be provided to force only them to be used.
# If options->debias is in the config, then it will choose a source that has items,
# at random, thereby balancing the selected sources, rather than biasing them
# based on the size of their catalogue.
sub _find_item {
    my ($self, $source_name_arg, $subsource_id_arg) = @_;

    my $schema = $self->{schema};

    my (@source_ids, @subsource_ids);
    if ( $source_name_arg && @$source_name_arg ) {
        @source_ids = map { $self->{sourcefactory}->get_id_for_name($_) } @$source_name_arg;
        @subsource_ids =
          ( $subsource_id_arg && @$subsource_id_arg )
          ? @$subsource_id_arg
          : ();
    } elsif ( $self->config->{options}{debias} ) {

        # Find a random source ID+subsource ID combination, so that we pick
        # equally from all museums To combat a slow query that goes through
        # everything with a group_by we instead manually check for one row of
        # each source to make sure that we're not picking an empty source
        my @source_ids_all;
        my @possible_sources    = $schema->resultset('Source')->search( { enabled => 1 } );
        my @possible_subsources = $schema->resultset('Subsource')->search( { enabled => 1 } );
        foreach my $source (@possible_sources) {
            # If we ever get a lot of sources/subsources this will start to explode.
            # Deal with that when it starts to become an issue.
            # TODO probably deal with it sooner:
            # select source_id, subsource_id from record where is_used=0 and is_eligible=1 and error=0 group by source_id, subsource_id;
            foreach my $subsource (undef, @possible_subsources) {
                my $row = $schema->resultset('Record')->find(
                    {
                        is_eligible  => 1,
                        is_used      => 0,
                        error        => 0,
                        source_id    => $source->id,
                        subsource_id => $subsource ? $subsource->id : undef,
                    },
                    {
                        rows => 1,
                    }
                );
                if ($row) {
                    push @source_ids_all, [ $source->id, $subsource ? $subsource->id : undef];
                }
            }
        }
        my $selected = $source_ids_all[ int rand @source_ids_all ];
        @source_ids  = $selected->[0];
        # Don't let undef leak into subsource_ids
        @subsource_ids = $selected->[1] ? $selected->[1] : ();
    }
    # See https://chaos.social/@isotopp/109976321513426564 for a discussion of
    # this approach.
    my $result = $schema->txn_do(
        sub {
            my $rs = $schema->resultset('Record')->find(
                {
                    is_eligible => 1,
                    is_used     => 0,
                    error       => 0,
                    @source_ids ? ( source_id => { -in => \@source_ids } ) : (),

                    # SQL::Abstract::Classic doesn't seem to like undef, so we
                    # work around it manually.
                    @subsource_ids
                    ? ( subsource_id => { -in => @subsource_ids } )
                    : (),
                },
                {
                    rows     => 1,
                    order_by => 'ordering',

                    #for      => \'update skip locked', # see monkey patched method above
                    for => \'update'
                    ,    # current db version can't handle 'skip locked', I need to upgrade anyway
                }
            );
            if ( !$self->testing && $rs ) {
                $rs->is_used(1);
                $rs->update;
            }
            return $rs;
        }
    );
    return $result;
}

# Queries the source for all the item details, munges them into a common format,
# and returns them.
sub _get_item_details {
    my ( $self, $item ) = @_;

    my $source_id = $item->source_id;
    my $harv      = $self->{sourcefactory}->get_harvester_for_id( $item->source_id,
        log       => sub { $self->_log(@_); },
        config    => $self->config->{source},
        resultset => $self->{schema}->resultset('Record'),
    );

    my $details = $harv->get_item_details( $item->record_id );
    return $details;
}

# Notes in the database that there was some error with a particular item
sub _mark_error {
    my ($self, $item, $error) = @_;

    $item->error(1);
    $item->error_details($error);
    $item->is_used(0); # because if there was an error, it probably wasn't used.
    $item->used_timestamp(undef);
    $item->update;
}

# Mark this item as used
sub _mark_sent {
    my ($self, $item) = @_;

    $item->is_used(1);
    $item->used_timestamp(\'NOW()');
    $item->update;
}

# Mark this item as not used
sub _mark_unsent {
    my ($self, $item) = @_;

    $item->is_used(0);
    $item->used_timestamp(undef);
    $item->update;
}

# Log something that's happened
sub _log {
    my ( $self, $level, $message ) = @_;

    my $ts = DateTime->now();
    push $self->{_logdetail}{entries}->@*, { level => uc($level), message => $ts . ' ' . $message };
    if ( lc($level) eq $ERROR ) {
        $self->{_logdetail}{level} = 'ERROR';
    }
    $self->{_logger}->log( $level, $message );
}

# Persist the logs into the database
sub _close_log {
    my ($self) = @_;

    my $content = JSON::MaybeXS->new( 'utf8' => 1 )->encode( $self->{_logdetail}{entries} );
    $self->{schema}->resultset('EventLog')->create(
        {
            source => 'publisher',
            level  => $self->{_logdetail}{level} // 'INFO',
            event  => $content,
        }
    );
}

has config => (
    is => 'ro',
    required => 1,
);

has testing => (
    is => 'ro',
    required => 0,
);

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Publisher

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;    # End of App::ExhibitExplorer

