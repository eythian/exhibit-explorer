package App::ExhibitExplorer::Maintenance;

use v5.34.0;
use strictures 2;

use Carp;
use Log::Log4perl;
use Log::Log4perl::Level;
use Moo;

use App::ExhibitExplorer::Schema;
use App::ExhibitExplorer::SourceFactory;

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::Maintenance - Contains functions to run maintenance on the database

=head1 VERSION

See L<App::ExhibitExplorer>

=cut

use App::ExhibitExplorer; our $VERSION = $App::ExhibitExplorer::VERSION;

=head1 SYNOPSIS

    use App::ExhibitExplorer::Maintenance;

    my $maint = App::ExhibitExplorer::Maintenance->new(config => $config);
    # Renumber all the database entries ordering values
    $maint->renumber();

=head1 CONFIGURATION

This takes the same configuration format as L<App::ExhibitExplorer::Harvester>.

=head1 SUBROUTINES/METHODS

=head2 new

    my $pub = App::ExhibitExplorer::Maintenance->new(config => $config);

Instantiate a new instance of the maintenance class, with the config.

=cut

sub BUILD {
    my ( $self, $args ) = @_;
    Log::Log4perl->easy_init($DEBUG);
    $self->{_logger} = Log::Log4perl->get_logger();
    my $db = $args->{config}{database};
    croak "Database configuration missing" unless $db;
    $self->{schema} =
      App::ExhibitExplorer::Schema->connect( 'dbi:' . $db->{type} . ':dbname=' . $db->{database},
        $db->{username}, $db->{password} );
    $self->{sourcefactory} = App::ExhibitExplorer::SourceFactory->new( schema => $self->{schema} );
}

=head2 renumber

    $maint->renumber();

Generates new ordering values for every record in the database. As records are
published, this leaves a gap in the start of ordering range, and newly added
records can have values in this gap. This ends up biasing selection towards
newly added items. By renumbering everything, this bias is removed. It will
change the order of future selections, but that doesn't generally matter.

=cut

sub renumber {
    my ($self) = @_;

    my $schema = $self->{schema};
    my $exception;
    eval {
        $schema->txn_do(
            sub {
                my $record_rs = $schema->resultset('Record')
                  ->search( { is_used => 0, is_eligible => 1, error => 0 } );
                my $total_count = $record_rs->count();
                $self->_log($INFO, "Beginning renumbering, $total_count records.");
                my $done_count = 0;
                my $modulo = int($total_count / 10);
                while (my $rec = $record_rs->next) {
                    $rec->ordering(rand());
                    $rec->update;
                    $done_count++;
                    if ($done_count % $modulo == $modulo-1) {
                        my $pc = int($done_count / $total_count * 100);
                        $self->_log($INFO, "$done_count records done: $pc%");
                    }
                }
            }
        );
        $self->_log($INFO, "Renumbering complete");
        1;
    } or do {
        my $error = $@ || 'Zombie error';
        $self->_log( $ERROR, "renumber() had an exception: $@" );
        $exception = $error;
    };
    $self->_close_log;
    die $exception if $exception;
}

# Log something that's happened
sub _log {
    my ( $self, $level, $message ) = @_;

    my $ts = DateTime->now();
    push $self->{_logdetail}{entries}->@*, { level => uc($level), message => $ts . ' ' . $message };
    if ( lc($level) eq $ERROR ) {
        $self->{_logdetail}{level} = 'ERROR';
    }
    $self->{_logger}->log( $level, $message );
}

# Persist the logs into the database
sub _close_log {
    my ($self) = @_;

    my $content = JSON::MaybeXS->new( 'utf8' => 1 )->encode( $self->{_logdetail}{entries} );
    $self->{schema}->resultset('EventLog')->create(
        {
            source => 'maintenance',
            level  => $self->{_logdetail}{level} // 'INFO',
            event  => $content,
        }
    );
}

has config => (
    is => 'ro',
    required => 1,
);

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Maintenance

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;

