package App::ExhibitExplorer::Harvester;

use v5.34.0;
use strictures 2;

use Carp;
use Log::Log4perl;
use Log::Log4perl::Level;
use Moo;
use JSON::MaybeXS;

use App::ExhibitExplorer::Schema;
use App::ExhibitExplorer::SourceFactory;

use namespace::clean;

=head1 NAME

App::ExhibitExplorer::Harvester - The harvester agent for Exhibit Explorer

=head1 VERSION

See L<App::ExhibitExplorer>

=cut

use App::ExhibitExplorer; our $VERSION = $App::ExhibitExplorer::VERSION;

=head1 SYNOPSIS

This starts ingesting all the records or identifiers from the supported museums
and adding any missing to the database.

    use App::ExhibitExplorer::Harvester;

    my $harv = App::ExhibitExplorer::Harvester->new(config => $config);
    my $status = $harv->harvest();
    if ($status->{error}) {
        die "Something failed: $status->{error}\n";
    }

=head1 CONFIGURATION

The constructor expects the following hashref as configuration:

    {
        database => {
            type     => 'mysql',
            database => 'databasename',
            username => 'username',
            password => 'password',
            host     => 'host',
        },
        source => {
            rijksmuseum => {
                key => 'api key',
            },
        },
        publisher => {
            console => {
                enabled => true,
            },
            mastodon => {
                enabled => false,
            },
        },
    }

=head1 SUBROUTINES/METHODS

=head2 new

    my $harv = App::ExhibitExplorer::Harvester->new(config => $config);

Instantiate a new instance of the harvester, with its configuration.

=cut

sub BUILD {
    my ( $self, $args ) = @_;
    Log::Log4perl->easy_init($DEBUG);
    $self->{_logger} = Log::Log4perl->get_logger();
    my $db = $args->{config}{database};
    croak "Database configuration missing" unless $db;
    $self->{schema} =
      App::ExhibitExplorer::Schema->connect( 'dbi:' . $db->{type} . ':dbname=' . $db->{database},
        $db->{username}, $db->{password} );
}

=head2 harvest

    my $status = $harv->harvest(sources => [qw(rijksmuseum)]);

Begin harvesting. If C<sources> is provided, only run for those ones. If not
provided, only run for the eligible sources in the database. C<subsources> can
be a list of subsource IDs (not names) that will be passed on to the source
module to do whatever with.

=cut

sub harvest {
    my ( $self, %args ) = @_;

    my $exception;
    eval {
        my %source_to_id;
        if ( $args{sources} ) {
            $source_to_id{$_} = -1 foreach $args{sources}->@*;
        }

        my $schema = $self->{schema};

        # The provided options can override the database
        if ( !%source_to_id ) {

            # Fetch the sources that we are going to try to run with
            my @sources = $schema->resultset('Source')->search( { enabled => 1 } );
            for my $s (@sources) {
                my $name = $s->name;
                $source_to_id{$name} = $s->id;
            }
        } else {
            my @sources = $schema->resultset('Source')->search();
            for my $s (@sources) {
                my $name = $s->name;
                if ( exists $source_to_id{$name} ) {
                    $source_to_id{$name} = $s->id;
                }
            }
            my @missing = grep { $source_to_id{$_} == -1 } keys %source_to_id;
            croak "Unknown source(s): " . join( ', ', @missing ) if @missing;
        }

        $self->_log( $INFO, "Starting harvest with " . join( ', ', keys %source_to_id ) );

        my $sf = App::ExhibitExplorer::SourceFactory->new( schema => $self->{schema} );
        for my $src ( keys %source_to_id ) {
            eval {
                my @subsources = $self->{schema}->resultset('Subsource')->search({
                    enabled => 1,
                    source_id => $source_to_id{$src},
                });
                my %h_params = (
                    log        => sub { $self->_log(@_); },
                    config     => $self->config->{source},
                    resultset  => $self->{schema}->resultset('Record'),
                    subsources => \@subsources,
                );

                # Select the source module we care about. This could be done in a more
                # flexible way, but unless I get dozens of modules, it's probably fine
                # like this.
                my $harv = $sf->get_harvester_for_name($src, %h_params);
                $self->_log( $INFO, "Running for '$src'" );
                $harv->harvest;
                $self->_log( $INFO, "Run finished for '$src'" );
                1;
            } or do {
                my $error = "harvesting for '$src' had an exception: $@";
                $self->_log( $ERROR, $error );
                $exception = $error;
            }
        }

        $self->_log( $INFO, "Finished" );
        1;
    } or do {
        my $error = "harvest() had an exception: $@";
        $self->_log( $ERROR, $error );
        $exception = $error;
    };
    $self->_close_log;
    die $exception if $exception;
}

# Log something that's happened
sub _log {
    my ( $self, $level, $message ) = @_;

    my $ts = DateTime->now();
    push $self->{_logdetail}{entries}->@*, { level => uc($level), message => $ts . ' ' . $message };
    if ( lc($level) eq $ERROR ) {
        $self->{_logdetail}{level} = 'ERROR';
    }
    $self->{_logger}->log( $level, $message );
}

# Persist the logs into the database
sub _close_log {
    my ($self) = @_;

    my $content = JSON::MaybeXS->new( 'utf8' => 1 )->encode( $self->{_logdetail}{entries} );
    $self->{schema}->resultset('EventLog')->create(
        {
            source => 'harvester',
            level  => $self->{_logdetail}{level} // 'INFO',
            event  => $content,
        }
    );
}

=head1 ATTRIBUTES

=head2 config

The config structure for the harvester.

=cut

has config => (
    is       => 'ro',
    required => 1,
);

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer::Harvester

You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (or here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1;    # End of App::ExhibitExplorer
