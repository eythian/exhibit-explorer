package App::ExhibitExplorer;

use 5.006;
use strict;
use warnings;

=head1 NAME

App::ExhibitExplorer - Ingests catalogue from museums and publishes random
items on a schedule

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

This particular module is a stub that doesn't do anything.

The modules that are useful are:

=over 4

=item L<App::ExhibitExplorer::Harvester>

This, along with its sub-modules, actually performs the harvesting from the
museums' catalogues.

=item L<App::ExhibitExplorer::Publisher>

This determines the item that is going to be published, fetches the data for
it, and then publishes it using whatever submodules are set up (for example,
L<App::ExhibitExplorer::Publisher::Mastodon>.)

=item L<App::ExhibitExplorer::Maintenance>

Contains maintenance functions, for example renumbering the ordering.

=back

=head1 AUTHOR

Robin Sheat, C<< <rsheat at cpan.org> >>

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc App::ExhibitExplorer


You can also look for information at:

=over 4

=item * Project repository (report bugs here)

L<https://gitlab.com/eythian/exhibit-explorer>

=item * RT: CPAN's request tracker (report bugs here)

L<https://rt.cpan.org/NoAuth/Bugs.html?Dist=App-ExhibitExplorer>

=item * Search CPAN

L<https://metacpan.org/release/App-ExhibitExplorer>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2023 by Robin Sheat.

This is free software, licensed under:

  The GNU General Public License, either version 3 of the License, or (at your
  option) any later version.

=cut

1; # End of App::ExhibitExplorer
