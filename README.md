# Exhibit Explorer

This is an application that does two things:

* Periodically (perhaps weekly) ingests the catalogue from a museum
* Periodically (perhaps every few hours) selects a random entry from the
  database, and posts it to a Mastodon account.

You can see it in action on its Mastodon account: <https://teh.entar.net/@ExhibitExplorer>

## Current Status

* Active Museums:
  * Rijksmuseum: <https://data.rijksmuseum.nl/>
  * Te Papa Tongarewa: <https://data.tepapa.govt.nz/docs/>
  * Metroplitan Museum of Art: <https://metmuseum.github.io/>
  * EICAS: <https://www.eicas.nl/>
  * DigitalNZ <https://digitalnz.org/>, using a bunch of interesting sources from there
* The publishing functionality:
  * Selecting a random entry
  * Fetching the catalogue details, images for it
  * Modules to publish to Mastodon
  * Publishing

## To-Do After Putting Live

* Adding more sources:
  * Suggestions welcome, open an issue in gitlab with details
* Adding more publishing destinations:
  * Telegram channel
* Where possible do incremental updates from sources rather than reingesting
  the whole lot, and just re-do the whole lot occasionally.

## Code Improvements

* Make selecting harvesting/publishing modules more flexible/pluggable.
* Fix the logging so it's not so terribly hacky - Log4perl should be able to
  have a custom log thingamy that works how I want.

# SEE ALSO

* [Museum::Rijksmuseum::Object](https://gitlab.com/eythian/museum-rijksmuseum-object)
* [Museum::TePapa](https://gitlab.com/eythian/museum-tepapa)
* [Museum::MetroplitanMuseumArt](https://gitlab.com/eythian/museum-metropolitanmuseumart)
* [Museum::EICAS](https://gitlab.com/eythian/museum-eicas)

# INSTALLATION

It's working and all, but you'll probably need help with all the undocumented
bits, like DB schema and config file format.

# SUPPORT AND DOCUMENTATION

Mostly not yet written.

# LICENSE AND COPYRIGHT

This software is Copyright (c) 2023-2024 by Robin Sheat.

This is free software, licensed under the GNU General Public License, either
version 3 of the License, or (at your option) any later version.

