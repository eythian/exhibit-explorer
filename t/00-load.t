#!perl
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 15;

BEGIN {
    use_ok( 'App::ExhibitExplorer' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Harvester' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Harvester::Dummy' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Harvester::Rijksmuseum' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Harvester::TePapa' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Harvester::Powerhouse' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Publisher' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Publisher::Console' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Publisher::Mastodon' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Schema ' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Schema::Result::EventLog' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Schema::Result::Record' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Schema::Result::Source' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::SourceFactory' ) || print "Bail out!\n";
    use_ok( 'App::ExhibitExplorer::Utils::Images' ) || print "Bail out!\n";
}

diag( "Testing App::ExhibitExplorer $App::ExhibitExplorer::VERSION, Perl $], $^X" );
